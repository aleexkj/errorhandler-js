/**
 * Manages errors to be used with res.negotiate
**/
class ErrorHandler {
  /**
   * @param status  -  Name of HTTP status code
   * @param message -  Message to send in response
   * @param errors  -  Desglosed errors
   */
  constructor(status, message, errors = [], errorDetail) {
    this.name = 'ErrorHandler';
    this.status = status;
    this.message = message || 'unexpectedError';
    this.errors = errors || [];
    if(!Array.isArray(this.errors)){
      throw new Error("Problem in error construction, errors sould be an array, found: ", errors);
    }
    this.errorDetail = errorDetail;

    const stack = new Error(status).stack.split('\n');

    this.trace = stack.slice(2, 5);
  }

  statusNumber() {
    const status = typeof this.status == 'number'? this.status: ErrorHandler.config.httpCodes[this.status];

    if (!status) {
      ErrorHandler.config.logging.warn('Missing code for ' + this.status + ' in config, sending 500');
      return 500;
    }

    return status;
  }

  generateBody(req) {
    const body = {
      errors: this.errors,
      message: this.message
    };
    ErrorHandler.config.customizeBody(this, body, req);

    return body;
  }

  static mapErrors(mappings) {
    return function(err) {
      const newError = mappings[err.name];

      if (newError) {
        throw newError;
      }

      throw err;
    };
  }

  static toErrorHandler(err, req) {
    const errorConstructor = ErrorHandler.config.constructors[err.name] || ErrorHandler.config.constructors[err.code];
    if(errorConstructor){
      let errorH;
      try{
        errorH = errorConstructor(err, req);
      }catch(constructorFailError){
        ErrorHandler.config.logging.error("Error in customized constructor for name:" + err.name + " code:" + err.code);
        ErrorHandler.config.logging.error(constructorFailError);
        return new ErrorHandler('serverError', ErrorHandler.config.unexpectedErrorMessage);
      }
      return errorH;
    }else{
      throw new TypeError(err.name + ' unknown');
    }
  }

  static negotiate(req, res){
    return (error)=>{
      return new Promise((resolve) => {
        if (error.name == 'ErrorHandler') {
          return resolve(error);
        }

        return resolve(ErrorHandler.toErrorHandler(error, req));
      })
        .catch(() => {
          ErrorHandler.config.logging.error('CANNOT MAP ERROR: ');
          ErrorHandler.config.logging.error(error);
          return new ErrorHandler('serverError',ErrorHandler.config.unexpectedErrorMessage);
        })
        .then((error) => {
          const body = error.generateBody(req);
          ErrorHandler.config.beforeResponse(error, body, req);
          res.status(error.statusNumber());
          return res.json(body);
        });
    }
  }
}

module.exports = ErrorHandler;
