/**
 * Manages errors for respose
 */
const ErrorHandler = require('./lib/ErrorHandler');
const lodash = require('lodash');

let config ={
  httpCodes: {
    invalid: 422,
    conflict: 409,
    badRequest: 400,
    forbidden: 403,
    unauthorized: 401,
    unavailableForLegalReasons: 451,
    failedDependency: 424,
    preconditionFailed: 412,
    unsupportedMediaType: 415,
    notImplemented: 501,
    badGateway: 502,
    gone: 410,
    tooManyRequests: 429,
    notFound: 404,
    serverError: 500,
    redirect: 307,
    teapot: 418,
    notAcceptable: 406,
    serviceUnavailable: 503,
    locked: 423
  },
  logging : console,
  beforeResponse: function(error, body, req){},
  customizeBody: function(error, body){},
  constructors: {},
  unexpectedErrorMessage: 'unexpected_error'
}

module.exports = function(configCustom) {
  configCustom = lodash.defaultsDeep(configCustom, config);
  if(!configCustom.logging){
    configCustom.logging = {
      silly:()=>{},
      debug:()=>{},
      info:()=>{},
      warn:()=>{},
      error:()=>{}
    };
  }
  ErrorHandler.config = configCustom;
  return ErrorHandler;
}
