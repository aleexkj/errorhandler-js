// const FactoryGirl = require('factory-girl');
// const path = require('path');
// const requireTree = require('require-tree');
// const factory = FactoryGirl.factory;
// const adapter = new FactoryGirl.SequelizeAdapter();
// const db = require('./db');
// const debug = require('debug')('bootstrap');
// const request = require('supertest');
// const should = require('should');
//
//
// factory.setAdapter(adapter);
//
// before(() => {
//   return db.sync()
//   .then(() => {
//     // For Sequelize usage
//     const folder = path.join(process.cwd(), 'test', 'factories');
//     const factories = requireTree(folder);
//     Object.keys(factories).forEach(key => {
//       factories[key](factory);
//     });
//     factory.models = models;
//     global.factory = factory;
//     global.request = request;
//     global.should = should;
//   })
//   .catch((err) => {
//     debug("Databse not syncronized");
//     debug(err);
//   })
// });
//
// after(() => {
//   db.drop();
// });
//
const debug = require('debug')('bootstrap');
const should = require('should');


/**
* Using Sequelize
**/
const db = require('./db');

/**
* Using Express
**/


before(() => {
  return db.sync()
  .then(() => {
    global.should = should;
    /**
    * Using Sequelize
    **/
    const FactoryGirl = require('factory-girl');
    const path = require('path');
    const requireTree = require('require-tree');
    const factory = FactoryGirl.factory;
    const adapter = new FactoryGirl.SequelizeAdapter();
    const folder = path.join(process.cwd(), 'test', 'factories');
    const factories = requireTree(folder);
    factory.setAdapter(adapter);


    Object.keys(factories).forEach(key => {
      factories[key](factory);
    });
    factory.models = models;
    global.factory = factory;
    /**
    * Using Express
    **/
    const bodyParser = require('body-parser');
    const express = require('express');
    const app = express();
    const controller = require('../controllers/AdminController'); //Import controllers
    app.use(bodyParser.json());
    app.use('',controller);
    app.use('*',function(req, res){
      return ErrorHandler.negotiate(req, res)(new ErrorHandler('notFound','route_not_found'));
    });
    const server = app.listen(3000);
  })
  .catch((err) => {
    debug("Databse not syncronized");
    debug(err);
    throw err;
  })
});

afterEach(() => {
  /**
  * Clean environment before each test
  */
  return db.drop()
  .then(() => {
    return db.sync();
  })
});
