/* global Admin */
const faker = require('faker');

module.exports = factory => {
  return factory.define('admin', Admin, {
    email: factory.seq('Admin.id', n => { return faker.internet.email(); }),
    password: factory.seq('Admin.id', n => { return faker.lorem.words(1); }),
    name: factory.seq('Admin.id', n => { return faker.name.firstName(); }),
    last_name: factory.seq('Admin.id', n => { return faker.name.lastName(); }),
    role: 'admin'
  });
};
