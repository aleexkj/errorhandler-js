/**
 * AdminController.js
 *
 */
const router = require('express').Router();

router.post('/admins', (req, res) => {
  return Admin.create({
       name: req.body.name,
       last_name: req.body.last_name,
       email: req.body.email,
       role: req.body.role,
       deactivated_at: req.body.deactivated ? Date.now() : 0
   })
   .then((admin) => {
     res.status(201);
     return res.json(admin.toJSON());
   })
   .catch(ErrorHandler.negotiate(req,res));
});

router.post('/admins/error', (req, res) => {
   return Admin.create({
        name: req.body.name,
        last_name: req.body.last_name,
        email: req.body.email,
        phone: req.body.phone,
        role: req.body.role,
        deactivated_at: req.body.deactivated ? Date.now() : 0
    })
    .then((admin) => {
      x = undefinedVariable;
      return res.json(admin.toJSON());
    })
    .catch(ErrorHandler.negotiate(req,res));
});

router.post('/admins/errorThrow', (req, res) => {
   return Admin.create({
        name: req.body.name,
        last_name: req.body.last_name,
        email: req.body.email,
        phone: req.body.phone,
        role: req.body.role,
        deactivated_at: req.body.deactivated ? Date.now() : 0
    })
    .then((admin) => {
      throw new ErrorHandler('badRequest','bad_request',[{fields:['name'],message:'required'}]);
      return res.json(admin.toJSON());
    })
    .catch(ErrorHandler.negotiate(req,res));
});

module.exports = router;
